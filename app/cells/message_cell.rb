class MessageCell < Cell::ViewModel
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::TranslationHelper

  builds do |model, options|
    if model.sender.is_a?(User)
      Message::UserMessageCell
    elsif model.sender.is_a?(Reservation)
      Message::ReservationMessageCell
    end
  end

  def show; render; end
  
  private

    def time_ago
      time_ago_in_words model.created_at
    end
end
