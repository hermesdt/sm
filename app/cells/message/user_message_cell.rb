class Message::UserMessageCell < MessageCell
  include SharemadAvatarHelper

  property :body, :sender

  private
    def sender_name
      return model.sender.name
    end
end