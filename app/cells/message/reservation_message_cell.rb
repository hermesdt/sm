class Message::ReservationMessageCell < MessageCell
  def show
    reservation = model.sender
    if reservation.status == 'pending'
      if options[:current_user].id == reservation.creator_id
        render 'creator/pending'
      elsif options[:current_user].id == reservation.owner_id
        render 'owner/pending'
      end
    elsif reservation.status == 'accepted'
      if options[:current_user].id == reservation.creator_id
        render 'creator/accepted'
      elsif options[:current_user].id == reservation.owner_id
        render 'owner/accepted'
      end
    elsif reservation.status == 'rejected'
      if options[:current_user].id == reservation.creator_id
        render 'creator/rejected'
      elsif options[:current_user].id == reservation.owner_id
        render 'creator/rejected'
      end
    end
  end

  private

    def owner_name
      model.sender.owner.name
    end

    def creator_name
      model.sender.creator.name
    end

    def listing
      model.sender.listing
    end

    def discard_link
      link_to t("reservation.discard"), reject_reservation_path(model.sender), class: "label round alert", method: :post, remote: true
    end

    def accept_link
      link_to t("reservation.accept"), accept_reservation_path(model.sender), class: "label round success", method: :post, remote: true
    end

    def pay_link
      link_to t("reservation.pay"), pay_reservation_path(model.sender), class: "label round success", method: :post, remote: true
    end

    def start_date
      model.sender.start_date
    end

    def end_date
      model.sender.end_date
    end
end