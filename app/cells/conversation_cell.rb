class ConversationCell < Cell::ViewModel
  include ActionView::Helpers::DateHelper
  include SharemadAvatarHelper

  property :last_message, :subject

  def show
    render
  end

  private
    def listing
      @listing ||= reservation.listing
    end

    def reservation
      @reservation ||= model.reservation
    end

    def subject
      truncate(last_message.body, :length => 200)
    end

    def link_to_conversation
      link_to truncate(listing.name, :length => 200),
        conversation_path(model), class: "title"
    end

    def avatar_url
      if @options[:current_user].id == model.reservation.creator_id
        super(model.reservation.owner)
      else
        super(model.reservation.creator)
      end
    end

    def originator_name
      if @options[:current_user].id == model.reservation.creator_id
        model.reservation.owner.name
      else
        model.reservation.creator.name
      end
    end
end
