class Dashboard::ListingsCell < BaseCell
  include ActionView::Helpers::NumberHelper

  property :name, :description, :image_data

  def show
    render
  end

  def empty
    render
  end

end
