class Dashboard::WaitingReservationCell < Cell::ViewModel
  builds do |model, options|
    res = if model.creator_id == options[:current_user].id
      Dashboard::WaitingReservation::CreatorCell
    elsif model.owner_id == options[:current_user].id
      Dashboard::WaitingReservation::OwnerCell
    end
  end

  def show; render; end
end