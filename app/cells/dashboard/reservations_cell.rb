class Dashboard::ReservationsCell < BaseCell
  include ActionView::Helpers::NumberHelper

  property :listing, :start_date, :end_date, :status, :amount, :rejected?

  def show
    render
  end

  def empty
    render
  end

end
