class Dashboard::SidebarCell < BaseCell
  include SharemadAvatarHelper

  property :email, :name

  def show
    render
  end

  def address
    [model.address, model.postal_code].compact.join(", ")
  end

end
