class Dashboard::WaitingReservation::OwnerCell < Dashboard::WaitingReservationCell
  include ActionView::Helpers::TranslationHelper

  def show
    render
  end

  private
    def message
      text = if model.pending?
        I18n.t("reservation_message.pending_owner",
          owner: model.owner.name,
          user: model.creator.name,
          from: l(model.start_date, format: :long),
          to: l(model.end_date, format: :long),
          item_link: link_to(model.listing.name, listing_path(model.listing)),
          discard_link: discard_link,
          accept_link: accept_link)
      elsif model.accepted?
        I18n.t("reservation_message.accepted_owner",
          creator: model.creator.name,
          from: l(model.start_date, format: :long),
          to: l(model.end_date, format: :long),
          item_link: link_to(model.listing.name, listing_path(model.listing)))
      elsif model.paid?
        I18n.t("reservation_message.accepted_owner",
          user: model.creator.name,
          from: l(model.start_date, format: :long),
          to: l(model.end_date, format: :long),
          item_link: link_to(model.listing.name, listing_path(model.listing)))
      end
        
      content_tag :p do
        text
      end
    end

    def discard_link
      link_to I18n.t("reservation.discard"), reject_reservation_path(model), class: "label round alert", method: :post, remote: true
    end

    def accept_link
      link_to I18n.t("reservation.accept"), accept_reservation_path(model), class: "label round success", method: :post, remote: true
    end
end