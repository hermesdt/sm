class Dashboard::WaitingReservation::CreatorCell < Dashboard::WaitingReservationCell
  include ActionView::Helpers::TranslationHelper
  include MarkdownHelper

  def show
    render
  end

  private
    def message
      text = if model.pending?
        I18n.t("reservation_message.pending_creator",
          owner: model.owner.name,
          creator: model.creator.name,
          from: l(model.start_date, format: :long),
          to: l(model.end_date, format: :long),
          item_link: link_to(model.listing.name, listing_path(model.listing)))
      elsif model.accepted?
        I18n.t("reservation_message.accepted_creator",
          owner: model.owner.name,
          creator: model.creator.name,
          from: l(model.start_date, format: :long),
          to: l(model.end_date, format: :long),
          item_link: link_to(model.listing.name, listing_path(model.listing)),
          pay_link: pay_link)
      elsif model.rejected?
        I18n.t("reservation_message.rejected_creator",
          user: model.owner.name,
          item_link: link_to(model.listing.name, listing_path(model.listing)))
      end

      content_tag :p do
        markdown(text)
      end
    end

    def pay_link_old
      link_to I18n.t("reservation.pay"), pay_reservation_path(model), class: "label round success", method: :post, remote: true
    end

    def pay_link
      link_to I18n.t("reservation.pay"), new_payment_path(reservation_id: model.id)
    end
end