class GetOrCreateMangoPayWalletService
  def initialize user
    @user = user
  end

  def run
    if wallet = @user.mango_pay_wallet
      wallet
    else
      wallet = create_wallet

      @user.create_mango_pay_wallet(data: wallet)
    end
  end

  def create_wallet
    MangoPay::Wallet.create({
      Owners: [@user.mango_pay_account_id],
      Description: "Wallet for user #{@user.id}",
      Currency: 'EUR',
      Tag: "Wallet for user #{@user.id}"
    })
  end
end