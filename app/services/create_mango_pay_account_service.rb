require 'ostruct'

class CreateMangoPayAccountService < OpenStruct

  def run
    account_params = MangoPay::NaturalUser.create(account_data)
    account_params.merge!(Tag: "Natural user for #{user.id}")
    
    user.create_mango_pay_account(data: account_params)
  end
end