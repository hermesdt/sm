class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  private
    # def edit_params
    #   params.require(:user).permit(:name, :email, :password, :phone, :lat, :lng, :address)
    # end

    def after_update_path_for(resource)
      edit_user_registration_path
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:account_update) do |u|
        u.permit(:lat, :lng, :address, :password, :password_confirmation,
          :current_password, :postal_code, image_data_attributes: [:raw_data, :id],
          bank_account_attributes:
            [:iban, :bic, :id,
              :city,
              :country] )
      end
    end

    def sign_up_params
      params.require(:user).permit(:name, :email, :password, :phone)
    end

    # By default we want to require a password checks on update.
    # You can overwrite this method in your own RegistrationsController.
    def update_resource(resource, params)
      if resource.provider == 'facebook'
        resource.update_without_password(params)
      else
        resource.update_with_password(params)
      end
    end
end