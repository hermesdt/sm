class ConversationsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_mailbox
  before_action :get_conversation, except: [:index, :empty_trash]
  before_action :get_box, only: [:index]
  before_action :check_reservation_paid, only: [:reply]

  def reply
    current_user.reply_to_conversation(@conversation, params[:body])
    flash[:success] = 'Reply sent'
    respond_with(@conversation, location: conversation_path)
  end

  def show
    if !@conversation.is_participant?(current_user)
      redirect_to root_path, flash: {error: t("flash.unauthorized")}
    end
  end

  def index
    if @box.eql? "inbox"
      @conversations = @mailbox.inbox
    elsif @box.eql? "sent"
      @conversations = @mailbox.sentbox
    else
      @conversations = @mailbox.trash
    end

    @conversations = @mailbox.conversations

    @conversations = @conversations.page(params[:page]).per(15)
  end

  def destroy
    @conversation.move_to_trash(current_user)
    flash[:success] = 'The conversation was moved to trash.'
    redirect_to conversations_path
  end

  def restore
    @conversation.untrash(current_user)
    flash[:success] = 'The conversation was restored.'
    redirect_to conversations_path
  end

  def empty_trash
    @mailbox.trash.each do |conversation|
      conversation.receipts_for(current_user).update_all(deleted: true)
    end
    flash[:success] = 'Your trash was cleaned!'
    redirect_to conversations_path
  end

  def mark_as_read
    @conversation.mark_as_read(current_user)
    flash[:success] = 'The conversation was marked as read.'
    redirect_to conversations_path
  end

  private
    def check_reservation_paid
      conversation = get_conversation
      originator = conversation.originator

      if (reservation = originator).is_a?(Reservation)
        if !reservation.paid?
          redirect_to conversation_path(conversation), flash: {error: t("conversation.not_paid_yet")}
        end
      end
    end

    def get_box
      if params[:box].blank? or !["inbox","sent","trash"].include?(params[:box])
        params[:box] = 'inbox'
      end
      @box = params[:box]
    end

    def get_conversation
      @conversation ||= @mailbox.conversations.find(params[:id])
    end

    def get_mailbox
      @mailbox ||= current_user.mailbox
    end
end