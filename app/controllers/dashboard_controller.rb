class DashboardController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @user = current_user
    @listings = @user.listings.order("id DESC")
    @waiting_reservations = Reservation.related_to_user(@user).
      waiting_status.order('status ASC').order("id DESC")

    @reservations = @user.reservations.final_status.order("id DESC")
  end
end