class ImagesController < ApplicationController
  def show
    @image_data = ImageData.find(params[:id])
    send_data @image_data.data
  end
end