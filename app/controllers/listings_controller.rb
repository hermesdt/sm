class ListingsController < ApplicationController
  respond_to :html
  before_filter :authenticate_user!, only: [:create, :edit, :update, :destroy]
  before_filter :get_listing, only: [:show, :edit, :update, :destroy]

  def show
    @reservation = Reservation.new(listing: @listing,
      start_date: params[:start_date], end_date: params[:end_date])
    respond_with @listing
  end

  def new
    @listing = Listing.new
    @listing.image_data = @listing.build_image_data
    respond_with @listing
  end

  def create
    @listing = Listing.new(listing_params.merge({user_id: current_user.id}))
    @listing.image_data ||= @listing.build_image_data

    if @listing.valid?
      @listing.save!
      if current_user.bank_account.nil?
        flash[:notice] = t('flash.listings.create.notice_without_bank_account')
      end
      UserNotifier.listing_created(current_user, @listing).deliver_now!
    end

    respond_with(@listing)
  end

  def edit
    if @listing.user_id == current_user.id
      respond_with @listing
    else
      not_authorized
    end
  end

  def update
    if @listing.user_id != current_user.id
      return not_authorized
    end

    attributes = listing_params
    if attributes[:image_data_attributes].keys == []
      attributes.delete(:image_data_attributes)
    end

    @listing.attributes = attributes
    if @listing.valid?
        @listing.save!
    end

    respond_with(@listing)
  end

  def destroy
    if @listing.user_id != current_user.id
      return not_authorized
    end

    @listing.destroy
    respond_with(@listing, location: dashboard_path)
  end

  private

    def get_listing
      @listing = Listing.find_by_id(params[:id])
      not_found if @listing.blank?
    end

    def listing_params
      params.require(:listing).permit(:name, :description, :daily_price,
        :lat, :lng, :address,
        :weekly_price, {image_data_attributes: [:raw_data]})
    end
end