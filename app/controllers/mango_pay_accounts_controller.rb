class MangoPayAccountsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_mango_pay_account_presence

  def new
    @next_step = params[:next_step]
    @account = Account.new
  end

  def create
    @next_step = params[:next_step]
    @account = Account.new(account_params.merge(email: current_user.email))

    if @account.valid?
      begin
        CreateMangoPayAccountService.new(user: current_user,
          account_data: @account.to_mango_params).run

        redirect_on_success
      rescue MangoPay::ResponseError => e
        e.errors.each do |key, error|
          if error.downcase == 'Birthday'
            @account.errors.add(:birthday, error)
          elsif error.downcase == 'FirstName'
            @account.errors.add(:first_name, error)
          elsif error.downcase == 'LastName'
            @account.errors.add(:last_name, error)
          elsif error.downcase == 'Nationality'
            @account.errors.add(:nationality, error)
          elsif error.downcase == 'CountryOfResidence'
            @account.errors.add(:country_of_residence, error)
          else
            @account.errors.add(:base, error)
          end
        end

        render action: :new
      end
    else
      render action: :new
    end
  end

  private
    def check_mango_pay_account_presence
      if current_user.mango_pay_account.present?
        redirect_to root_path, flash: {error: 'You already have an account'}
      end
    end

    def redirect_on_success
      if params[:next_step].present?
        uri = URI.parse(params[:next_step])
        redirect_to uri.path + "?" + (uri.query || '')
      else
        redirect_to root_path
      end
    end

    def account_params
      params.require(:account).permit(:first_name,
        :'birthday(1i)', :'birthday(2i)', :'birthday(3i)',
        :last_name, :nationality, :country_of_residence)
    end
end