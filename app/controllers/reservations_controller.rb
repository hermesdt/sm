class ReservationsController < ApplicationController
  before_filter :authenticate_user!

  def create
    listing = Listing.find(reservation_params[:listing_id])

    begin
      reservation = CreateReservationContext.new(current_user, listing, reservation_params).call
      if reservation.valid? && reservation.persisted?

        ReservationNotifier.created_owner(reservation.owner, reservation).deliver_now!
        ReservationNotifier.created_creator(reservation.creator, reservation).deliver_now!

        respond_with(reservation, location: dashboard_path)
      else
        errors = reservation.errors.full_messages
        redirect_to listing_path(listing), flash: {error: errors.join("\n")}
      end
    rescue CreateReservationContext::SameUserException => e
      redirect_to listing_path(listing), flash: {error: 'Error'}
    rescue CreateReservationContext::ItemReservedException => e
      redirect_to listing_path(listing,
        start_date: reservation_params[:start_date],
        end_date: reservation_params[:end_date]), flash: {error: I18n.t("reservations.reserved")}
    rescue ActiveRecord::RecordInvalid => e
      redirect_to listing_path(listing), flash: {error: e.to_s}
    end
  end

  def request_payment
    @reservation = current_user.reservations.find(params[:id])
  end

  def accept
    @reservation = current_user.reservation_requests.find(params[:id])
    @reservation.accepted!

    ReservationNotifier.accepted_owner(@reservation.owner, @reservation).deliver_now!
    ReservationNotifier.accepted_creator(@reservation.creator, @reservation).deliver_now!

    respond_to do |format|
      format.html { redirect_to dashboard_path }
      format.js { render :text => "window.location.reload();" }
    end
  end

  def reject
    @reservation = current_user.reservation_requests.find(params[:id])
    @reservation.rejected!

    ReservationNotifier.rejected_owner(@reservation.owner, @reservation).deliver_now!
    ReservationNotifier.rejected_creator(@reservation.creator, @reservation).deliver_now!

    respond_to do |format|
      format.html { redirect_to dashboard_path }
      format.js { render :text => "window.location.reload();" }
    end
  end

  private
    def reservation_params
      params.require(:reservation).permit(:listing_id, :start_date, :end_date)
    end
end