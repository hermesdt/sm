class RootController < ApplicationController
  def index
  end

  def change_locale
    locale = params[:locale]
    if I18n.available_locales.include?(locale.to_sym)
      I18n.locale = locale
      session[:locale] = locale
      redirect_to root_path
    else
      redirect_to root_path
    end
  end
end