class ContentsController < ApplicationController
  def section
    render params[:section]
  end
end