class InitialsController < ApplicationController
  def show
    if @user = User.find_by_id(params[:id])
      response.content_type = 'image/svg+xml'
      render :text =>  Initial.new(name: @user.name, seed: @user.id).generate
    else
      not_found
    end
  end
end