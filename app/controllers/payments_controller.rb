class PaymentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_mango_pay_account_presence
  before_filter :get_reservation

  def new
    @card = Card.new
  end

  def create
    @card = Card.new(card_params)
    if @card.valid?
      begin
        CreatePaymentContext.new(reservation: @reservation, card: @card).call

        redirect_to dashboard_path, flash: {success: t("reservations.paid")}
      rescue ChargeReservationCreatorContext::MangoPayResponseError => e
        Rails.logger.error e.message
        @card.errors.add(:base, e.message)

        render :action => :new
      end
    else
      render :action => :new
    end
  end

  private
    def check_mango_pay_account_presence
      if current_user.mango_pay_account.blank?
        redirect_to new_mango_pay_account_path(
          :next_step => new_payment_path(reservation_id: params[:reservation_id])
        )
      end
    end

    def get_reservation
      @reservation = Reservation.find(params[:reservation_id])
      raise StandardError.new(
        "current user (#{current_user.id}) is not reservation (#{@reservation.id}) creator (#{@reservation.creator_id})"
        ) if current_user.id != @reservation.creator_id

      return redirect_to dashboard_path,
        flash: {success: t("reservations.already_paid")} if @reservation.paid?

      return redirect_to root_path({flash: {error: 'Uknown reservation'}}) if @reservation.nil?

      @reservation
    end

    def card_params
      params.require(:card).permit(:number, :cvv, :expiration_month, :expiration_year)
    end
end