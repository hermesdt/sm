require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  around_filter :set_locale

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def not_authorized
    redirect_to root_path, flash: {error: t("flash.unauthorized")}
  end

  def set_locale
    initial_locale = I18n.locale
    begin
      I18n.locale = session[:locale] || I18n.default_locale
      yield
    ensure
      I18n.locale = initial_locale
    end
  end
end
