class SearchesController < ApplicationController
  respond_to :json, only: [:get_distances]
  def index
    @search = Search.new(search_params)
    @listings = SearchListingsContext.new(@search).call
  end

  def get_distances
    @result = if params[:lng] && params[:lat]
      longitude = Float(params[:lng])
      latitude = Float(params[:lat])

      locations = ListingLocation.
        limit(10_000). # mongoid forces to use a limit
        geo_near([longitude, latitude]).
        distance_multiplier(6378.1).
        spherical.
        unique

      locations
    else
      Listing.none
    end

    respond_with(locations)
  end

  private
    def search_params
      params[:search] ||= {somethingtopasstherequire: 'x'}
      params.require(:search).permit(:q, :lat, :lng, :start_date, :end_date, :address)
    end
end