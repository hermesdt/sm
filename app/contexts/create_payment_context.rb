class CreatePaymentContext < OpenStruct
  using Mailboxer::UserWithReservation

  class NotAcceptedStatusError < StandardError; end

  def call
    check_status!
    reservation.attributes = reservation_attributes

    if reservation.valid?
      charge_reservation_creator!
      reservation.save!

      send_message
    end
  end

  def check_status!
    if reservation.status != 'accepted'
      raise NotAcceptedStatusError, "not in accepted status (current status: #{reservation.status}), reservation_id: #{reservation.id}"
    end
  end

  def reservation_attributes
    {
      payment_data: {
      },
      status: Reservation.statuses[:paid]
    }
  end

  def charge_reservation_creator!
    ChargeReservationCreatorContext.new(reservation: reservation, card: card).call
    reservation.paid!
  end

  def get_card
    Card.new(card_params)
  end

  def send_message
    # start conversation
    message = reservation.creator.send_message reservation, reservation.owner,
      I18n.t("reservation.paid.body",
        creator: reservation.creator.name,
        owner: reservation.owner.name,
        item: reservation.listing.name),
      I18n.t("reservation.paid.subject", item: reservation.listing.name)

    ReservationNotifier.paid_creator(reservation.creator, reservation).deliver_now!
  end
end