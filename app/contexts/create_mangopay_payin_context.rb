class CreateMangopayPayinContext
  def initialize source_account, destination_account, amount
    @source_account = source_account
    @destination_account = destination_account
    @amount = amount
  end

  def call
    MangoPay::Wallet
  end
end