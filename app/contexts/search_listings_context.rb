class SearchListingsContext
  # we are going to use an inefficent query by now
  # it could be improved taken ideas from http://stackoverflow.com/questions/15939399/the-core-of-a-reservation-system-finding-an-unreserved-item-efficiently

  attr_reader :search

  def initialize search
    @search = search
  end

  def call
    reserved_ids = get_reserved_ids
    listings = search_listings(reserved_ids)
    filter_by_distance(listings)
  end

  def filter_by_distance listings
    if search.lat.present? && search.lng.present?
      search_loc = Geokit::LatLng.new(search.lat, search.lng)
      listings.select do |listing|
        listing_loc = Geokit::LatLng.new(listing.lat, listing.lng)

        valid_distance_to_search?(search_loc, listing_loc)
      end
    else
      listings
    end
  end

  def valid_distance_to_search? search_loc, listing_loc
    distance = Geokit::LatLng.distance_between(search_loc, listing_loc,
      units: :kms, formula: :sphere)

    distance <= max_distance_allowed
  end

  def max_distance_allowed
    @max_distance ||= Setting.get_max_search_distance
  end

  def get_reserved_ids
    reservations = Reservation.all
    reservations = if search.start_date.present? && search.end_date.present?
      reservations.where("(start_date <= ? and ? <= end_date) or 
        (? <= start_date and start_date <= ?)",
        search.start_date, search.start_date, search.start_date, search.end_date)
    else
      reservations.none
    end
    reserved_ids = reservations.map(&:listing_id).uniq
    reserved_ids = [0] if reserved_ids.empty?
    reserved_ids
  end

  def search_listings reserved_ids
    listings = Listing.all
    if search.q.present?
      listings = listings.
        where("lower(listings.name) like ? or lower(listings.description) like ?", *["%#{search.q.downcase}%"]*2)
    end

    if reserved_ids.any?
      listings = listings.where("id not in (?)", reserved_ids)
    end
  end
end