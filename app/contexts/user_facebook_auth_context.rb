class UserFacebookAuthContext < Struct.new(:auth_params)

  def call
    Rails.logger.info auth_params.inspect
    user_from_omniauth(auth_params)
  end

  private
    def user_from_omniauth(auth_params)
      User.where(provider: auth_params.provider, uid: auth_params.uid).first_or_create do |user|
        user.email = auth_params.info.email
        user.password = Devise.friendly_token[0,20]
        user.name = auth_params.info.name   # assuming the user model has a name
        user.image = auth_params.info.image # assuming the user model has an image
      end
    end

end