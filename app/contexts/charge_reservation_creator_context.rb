class ChargeReservationCreatorContext < OpenStruct
  class CardNotValidError < StandardError; end
  class MangoPayResponseError < StandardError
    def initialize code
      @code = if match = code.match(/errorCode=(.*)/)
        match[1]
      else
        :unkown
      end
    end

    def to_s
      super + " (#{I18n.t("mangopay.errors.#{@code}")})"
    end

    def message
      I18n.t("mangopay.errors.#{@code}")
    end
  end

  def call
    card_data = get_card_data

    data = {
      data: card_data['PreregistrationData'],
      accessKeyRef: card_data['AccessKey'],
      cardNumber: card.number,
      cardExpirationDate: card.expiration_date,
      cardCvx: card.cvv}

    res = post_to_card_registration_url(card_data['CardRegistrationURL'], data)
    card_data['RegistrationData'] = res.body

    card_data = update_mango_card(card_data['Id'], card_data['RegistrationData'])

    to_wallet = get_owner_wallet
    payin_response = create_payin(to_wallet, card_data['CardId'])
    Rails.logger.error "[PAYIN_RESPINSE] #{payin_response.inspect}"
    check_payin_response! payin_response
    payin_response
  end

  def check_payin_response! payin_response
    if payin_response["Status"] == 'FAILED'
      raise MangoPayResponseError.new(payin_response["ResultCode"])
    end
  end

  def post_to_card_registration_url url, data
    Rails.logger.error "[CHARGE_RESERVATION] proceding to call: #{url}"

    options_mask = OpenSSL::SSL::OP_NO_SSLv2 + OpenSSL::SSL::OP_NO_SSLv3 +
      OpenSSL::SSL::OP_NO_COMPRESSION
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme == "https"
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      # http.ssl_options = options_mask
    end

    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_form_data(data)

    res = http.request(request)

    if res.is_a?(Net::HTTPOK) && res.body.start_with?("data=")
      res
    else
      Rails.logger.error "[MangoPayError]: " + res.body.inspect
      raise MangoPayResponseError.new(res.body), [res, data]
    end
  end

  def update_mango_card card_id, card_registration_data
    MangoPay::CardRegistration.update(card_id, {
      RegistrationData: card_registration_data
    })
  end

  def get_owner_wallet
    GetOrCreateMangoPayWalletService.new(reservation.owner).run
  end

  def create_payin to_wallet, card_id
    MangoPay::PayIn::Card::Direct.create({
      AuthorId: reservation.creator.mango_pay_account_id,
      CreditedUserId: to_wallet.data['Owners'][0],
      CreditedWalletId: to_wallet.data['Id'],
      DebitedFunds: { Currency: 'EUR', Amount: reservation_amount },
      Fees: { Currency: 'EUR', Amount: 0 },
      CardType: 'CB_VISA_MASTERCARD',
      CardId: card_id,
      SecureModeReturnURL: 'http://test.com',
      Tag: "PayIn:Card:Direct #{reservation.creator_id}"
    })
  end

  def reservation_amount
    "%i" % (reservation.amount.to_f * 100)
  end

  def get_card_data
    MangoPay::CardRegistration.create({
      UserId: reservation.creator.mango_pay_account_id,
      Currency: 'EUR',
      Tag: "buyer card #{reservation.creator_id}"
    })
  end
end