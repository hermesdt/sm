class CreateReservationContext
  attr_reader :current_user, :listing, :reservation_params, :start_date, :end_date

  class SameUserException < StandardError; end
  class ItemReservedException < StandardError; end

  def initialize current_user, listing, reservation_params
    @current_user = current_user
    @reservation_params = reservation_params
    @listing = listing

    @end_date = reservation_params[:end_date].to_date
    @start_date = reservation_params[:start_date].to_date
  end

  def call
    if listing.user_id == current_user.id
      raise SameUserException.new("creator user (#{listing.user_id}) and current_user (#{current_user.id}) are the same")
    end

    if item_reserved?(listing)
      raise ItemReservedException.new("item #{listing.id} already reserved for #{start_date} - #{end_date}")
    end

    reservation = create_reservation(listing)

    reservation
  end

  def item_reserved? listing
    Reservation.where(listing_id: listing.id).
      where("status in (?)", Reservation.reserved_statuses).
      where("(start_date <= ? and ? <= end_date) or (? <= start_date and start_date <= ?)",
        start_date, start_date, start_date, end_date).any?
  end

  def create_reservation listing

    weeks = (end_date - start_date).to_i / 7
    days = (end_date - start_date).to_i % 7
    amount = days * listing.daily_price + weeks * listing.weekly_price

    reservation = current_user.
      reservations.
      create(reservation_params.
        merge(owner_id: listing.user_id, amount: amount, status: Reservation.statuses[:pending]))

    reservation
  end
end