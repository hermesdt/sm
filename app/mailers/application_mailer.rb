class ApplicationMailer < ActionMailer::Base
  default from: "Wavyy <info@wavyy.co>"
  layout 'mailer'
end