class UserNotifier < ApplicationMailer
  def signup user
    @user = user

    mail(to: user.email,
      bcc: 'alvaro@codeender.com',
      subject: I18n.t("mailers.user_notifier.signup.subject"))
  end

  def listing_created user, listing
    @user = user
    @listing = listing

    mail(to: @user.email,
    bcc: 'alvaro@codeender.com',
    subject: I18n.t("mailers.user_notifier.signup.subject")) do |format|
      format.html { render layout: 'user_notifier' }
      format.text
    end
  end
end
