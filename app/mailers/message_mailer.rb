class MessageMailer < ApplicationMailer
  #Sends and email for indicating a new message or a reply to a receiver.
  #It calls new_message_email if notifing a new message and reply_message_email
  #when indicating a reply to an already created conversation.
  def self.send_email(message, receiver)
    if message.conversation.messages.size > 1
      reply_message_email(message, receiver)
    else
      new_message_email(message, receiver)
    end
  end

  #Sends an email for indicating a new message for the receiver
  def new_message_email(message, receiver)
    @message  = message
    @receiver = receiver
    set_subject(message)
    mail :to => receiver.send(Mailboxer.email_method, message),
         :subject => @subject,
         :template_name => 'new_message_email'
  end

  #Sends and email for indicating a reply in an already created conversation
  def reply_message_email(message, receiver)
    @reservation = message.conversation.reservation
    @receiver = receiver

    message_from = if message.sender == @reservation.creator
      @reservation.owner.name
    else
      @reservation.creator.name
    end

    @body = if message.sender == @reservation.creator
      t("mailers.reply_message.to_owner.body",
        owner: @reservation.owner.name,
        creator: @reservation.creator.name,
        message: message.body,
        conversation_url: conversation_url(message.conversation))
    else
      t("mailers.reply_message.to_creator.body",
        owner: @reservation.owner.name,
        creator: @reservation.creator.name,
        message: message.body,
        conversation_url: conversation_url(message.conversation))
    end

    set_subject(message)
    mail :to => receiver.send(Mailboxer.email_method, message),
         :subject => t('global.message_from', :name => message_from),
         :template_name => 'reply_message_email'
  end

  private
    def set_subject(container)
      @subject  = container.subject.html_safe? ? container.subject : strip_tags(container.subject)
    end

    def strip_tags(text)
      ::Mailboxer::Cleaner.instance.strip_tags(text)
    end
end