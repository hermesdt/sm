class ReservationNotifier < ApplicationMailer
  layout 'user_notifier'

  def created_owner user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.created_owner.subject',
        item: reservation.listing.name))
  end

  def created_creator user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.created_creator.subject',
        item: reservation.listing.name))
  end

  def accepted_owner user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.accepted_owner.subject',
        item: reservation.listing.name))
  end

  def accepted_creator user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.accepted_creator.subject',
        item: reservation.listing.name))
  end

  def rejected_owner user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.rejected_owner.subject',
        item: reservation.listing.name))
  end

  def rejected_creator user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.rejected_creator.subject',
        item: reservation.listing.name))
  end

  def paid_owner user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.paid_owner.subject',
        item: reservation.listing.name))
  end

  def paid_creator user, reservation
    @reservation = reservation

    mail(to: user.email,
      subject: I18n.t('mailers.reservation_notifier.paid_creator.subject',
        item: reservation.listing.name))
  end
end
