jQuery(document).ready(function($){
    var pickers = $(".pick-date").pickadate({
        today: '',
        clear: '',
        close: '',
        min: true,
        format: 'dd/mm/yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenName: true
    });

    pickers.each(function(index, node){
        var picker = $(node).pickadate('picker');

        picker.on('close', function(evt) {
            var element = this.$node;
            var type = element.attr("date_picker_type");

            var otherType = type == 'from' ? 'to' : 'from';
            var other = element.parent().parent().find("[date_picker_type='"+ otherType +"']");
            var formatedElement = moment(element.val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var formatedOther = moment(other.val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

            if(moment(formatedElement).isSame(formatedOther) ||
                !moment(formatedElement).isValid() ||
                !moment(formatedOther).isValid()){
                return true;
            }

            if(type == 'from'){
                var from = element, to = other;
                var formatedFrom = formatedElement, formatedTo = formatedOther;

                if(!moment(formatedFrom).isBefore(formatedTo)){
                    other.pickadate('picker').
                        set('select', moment(formatedElement).format('DD/MM/YYYY'))
                    other.pickadate("picker").open(true);
                }
            }else if(type == 'to'){
                var from = other, to = element;
                var formatedFrom = formatedOther, formatedTo = formatedElement;

                if(!moment(formatedTo).isAfter(formatedFrom)){
                    other.pickadate("picker").
                        set('select', moment(formatedElement).format('DD/MM/YYYY'))
                    other.pickadate("picker").open(true);
                }
            }
        });
    });

    
});