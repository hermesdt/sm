module Admin::Resources::DataTypes::SharemadHelper
  def table_binary_field(attribute, item)
    if item.is_a?(ImageData) && attribute == 'data'
      image_tag(image_path(item), size: '200x200')
    end
  end

  def table_money_field(attribute, item)
    if field = item.send(attribute)
      number_to_currency(field)
    end
  end

  def display_money(item, attribute)
    if field = item.send(attribute)
      number_to_currency(field)
    end
  end

  def table_inet_field(attribute, item)
    table_string_field(attribute, item)
  end

  def display_inet(item, attribute)
    display_string(item, attribute)
  end

  def display_json(item, attribute)
    display_string(item, attribute)
  end
end