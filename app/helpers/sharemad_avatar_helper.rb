module SharemadAvatarHelper
  include GravatarImageTag

  def avatar_url object
    if object.image_data
      image_path(object.image_data.id)
    elsif object.image?
      object.image
    else
      initial_path(object.id)
    # elsif object.email?
    #   gravatar_image_url(object.email, {default: "http://sharemad.herokuapp.com/sharemad_profile.png"})
    end
  end
end