module Mailboxer::UserWithReservation
  refine User do

    def send_message reservation, recipients, msg_body, subject, sanitize_text=true, attachment=nil, message_timestamp = Time.now
      receipt = super(recipients, msg_body, subject, sanitize_text, attachment, message_timestamp)
      conversation = receipt.message.conversation
      conversation.update_attribute("reservation_id", reservation.id)

      receipt
    end
  end
end