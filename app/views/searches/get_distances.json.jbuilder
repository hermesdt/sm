json.array! @result do |result|
  json.id result.listing_id
  json.distance result["geo_near_distance"].round(1)
end