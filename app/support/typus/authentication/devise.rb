module Typus
  module Authentication
    module Devise

      protected

      include Base

      def admin_user
        send("current_#{Typus.user_class_name.underscore}")
      end

      def authenticate
        if user_signed_in?
          if !current_user.has_role?(:admin)
            Rollbar.error("user #{current_user.id} (#{current_user.email}) tried to login in admin panel")
            sign_out(current_user)
            redirect_to root_path, flash: {error: 'Attempt to access to admin panel notified'}
          else
            @admin_user = current_user
          end
        else
          redirect_to root_path, flash: {error: 'Attempt to access to admin panel notified'}
        end
      end

    end
  end
end