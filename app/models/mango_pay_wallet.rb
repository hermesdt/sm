# == Schema Information
#
# Table name: mango_pay_wallets
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  data       :jsonb            not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_mango_pay_wallets_on_user_id  (user_id)
#

class MangoPayWallet < ActiveRecord::Base
  belongs_to :user
  
  validates :data, presence: true
end
