# == Schema Information
#
# Table name: reservations
#
#  id           :integer          not null, primary key
#  creator_id   :integer          not null
#  owner_id     :integer          not null
#  status       :integer          default(0), not null
#  listing_id   :integer          not null
#  start_date   :date             not null
#  end_date     :date             not null
#  amount       :money            not null
#  created_at   :datetime
#  updated_at   :datetime
#  payment_data :json             default({}), not null
#  transfered   :boolean          default(FALSE)
#  deleted_at   :datetime
#
# Indexes
#
#  index_reservations_on_creator_id             (creator_id)
#  index_reservations_on_creator_id_and_status  (creator_id,status)
#  index_reservations_on_end_date               (end_date)
#  index_reservations_on_listing_id             (listing_id)
#  index_reservations_on_owner_id               (owner_id)
#  index_reservations_on_owner_id_and_status    (owner_id,status)
#  index_reservations_on_start_date             (start_date)
#  index_reservations_on_status                 (status)
#

class Reservation < ActiveRecord::Base
  # acts_as_messageable
  acts_as_paranoid
  
  enum status: [ :pending, :accepted, :rejected, :paid ]
  def self.reserved_statuses
    [Reservation.statuses["accepted"], Reservation.statuses["paid"]]
  end

  scope :related_to_user, ->(user){ where("creator_id = ? or owner_id = ?", user.id, user.id) }
  scope :waiting_status, ->{ where("status in (?)", [self.statuses[:pending], self.statuses[:accepted]]) }
  scope :final_status, ->{ where("status in (?)", [self.statuses[:rejected], self.statuses[:paid]]) }

  validates :start_date, :end_date, :owner_id, :creator_id, :amount, :status, :listing_id, presence: true

  validate :validate_dates

  belongs_to :owner, class_name: 'User'
  belongs_to :creator, class_name: 'User'
  belongs_to :listing
  has_one :conversation, class_name: 'Mailboxer::Conversation', dependent: :destroy

  def validate_dates
    if start_date > end_date
      self.errors.add(:start_date,
        I18n.t("activerecord.errors.messages.date_interval",
          {start_date: self.start_date, end_date: self.end_date}))
    end
  end
end
