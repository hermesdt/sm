# == Schema Information
#
# Table name: settings
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  value      :text             not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_settings_on_name  (name)
#

class Setting < ActiveRecord::Base
  FEE_KEY = 'fee'
  MAX_SEARCH_DISTANCE = 'max_search_distance'

  validates :name, :value, presence: true

  def self.[](name)
    Rails.logger.error "[Setting]: Trying to access to value #{name}"
    Setting.where(name: name).first.try(:value)
  end

  def self.[]=(name, value)
    setting = Setting.where(name: name).first

    if setting
      setting.attributes = {value: value}
      setting.save!
    else
      Setting.create!(name: name, value: value)
    end

    value
  end

  def self.get_fee
    Setting[FEE_KEY].to_f
  end

  def self.get_max_search_distance
    Setting[MAX_SEARCH_DISTANCE].to_f
  end
end
