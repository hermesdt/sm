# == Schema Information
#
# Table name: mango_pay_accounts
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  data       :jsonb            not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_mango_pay_accounts_on_user_id  (user_id)
#

class MangoPayAccount < ActiveRecord::Base
  belongs_to :user
  validates :data, presence: true
end
