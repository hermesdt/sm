require 'ostruct'

class Card < OpenStruct
  include ActiveModel::Validations

  validates :number, :cvv, :expiration_month, :expiration_year, presence: true
  validates :expiration_month, :expiration_year, numericality: { only_integer: true }

  def expiration_date
    if expiration_month.present? && expiration_year.present?
      month = expiration_month.to_i
      month = month > 9 ? month.to_s : "0#{month}"
      year = expiration_year.to_i.to_s[-2..-1]

      month + year
    else
      ''
    end
  end
end