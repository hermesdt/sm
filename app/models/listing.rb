# == Schema Information
#
# Table name: listings
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  user_id      :integer
#  description  :text             not null
#  daily_price  :money            not null
#  weekly_price :money            not null
#  created_at   :datetime
#  updated_at   :datetime
#  lat          :float            not null
#  lng          :float            not null
#  address      :string           not null
#  deleted_at   :datetime
#
# Indexes
#
#  index_listings_on_user_id  (user_id)
#

class Listing < ActiveRecord::Base
  acts_as_paranoid

  has_one :image_data, as: :imageable
  belongs_to :user

  validates :name, :description, :daily_price,
    :weekly_price, :lat, :lng, :address, presence: true

  has_many :reservations, dependent: :destroy
  validates :image_data, presence: true
  after_save :update_mongo_location
  before_destroy :remove_listing_location

  accepts_nested_attributes_for :image_data

  def get_listing_location
    listing_location = ListingLocation.where(listing_id: self.id).first
    listing_location ||= ListingLocation.create(listing_id: self.id)

    listing_location
  end

  private
    def update_mongo_location
      if lat && lng
        listing_location = get_listing_location
        listing_location.location = [lng, lat]
        listing_location.save
      end
    end

    def remove_listing_location
      get_listing_location.destroy
    end
end
