# == Schema Information
#
# Table name: image_data
#
#  id             :integer          not null, primary key
#  data           :binary           not null
#  content_type   :string           not null
#  imageable_id   :integer          not null
#  imageable_type :string           not null
#  created_at     :datetime
#  updated_at     :datetime
#

class ImageData < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  validates :data, :content_type, :presence => true

  validate :validate_raw_data
  validate :validate_data_geometry
  before_save :crop_avatar
  
  attr_reader :raw_data

  def validate_raw_data
    if self.data.blank?
      self.errors.add(:raw_data, :blank)
    end
  end

  def raw_data= input
    self.data = input.read
    self.content_type = input.content_type
  end

  private
    def validate_data_geometry
      # return if !new_record?
      
      # image = Magick::Image.from_blob(self.data).first
      # if imageable.is_a?(User)
      #   min = [image.columns, image.rows].min

      #   if image.columns != image.rows
      #     self.errors.add :data, I18n.t("activerecord.errors.messages.need_to_be_square")
      #   end
      # end
    end

    def crop_avatar
      return if self.data.blank?

      image = Magick::Image.from_blob(self.data).first

      if imageable.is_a?(User) && image.columns != image.rows
        min = [image.columns, image.rows].min
        self.data = image.crop(Magick::CenterGravity, min, min).to_blob
      end
    end
end
