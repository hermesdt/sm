# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  provider               :string
#  uid                    :string
#  name                   :string
#  image                  :text
#  phone                  :integer
#  deleted_at             :datetime
#  lat                    :float
#  lng                    :float
#  address                :string
#  postal_code            :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  acts_as_messageable
  acts_as_paranoid

  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:facebook]

  validates :name, :email, presence: true
  validates :postal_code, numericality: { only_integer: true }, allow_blank: true
  # validate :validate_ie_email

  has_many :listings, dependent: :destroy
  has_many :reservations, foreign_key: :creator_id
  has_many :reservation_requests, foreign_key: :owner_id, class_name: 'Reservation'
  has_one :bank_account, dependent: :destroy
  has_one :mango_pay_wallet, dependent: :destroy
  has_one :mango_pay_account, dependent: :destroy
  has_one :image_data, as: :imageable
  
  accepts_nested_attributes_for :image_data
  accepts_nested_attributes_for :bank_account

  after_create :send_welcome_email

  def validate_ie_email
    if self.new_record? && (email = self.email)
      if !email.match(/@student.ie.edu\Z/)
        self.errors.add :email, I18n.t("activerecord.errors.messages.only_ie_emails")
      end
    end
  end

  def mailboxer_email(object)
    email
  end

  def applications
    Typus.applications
  end

  def application(name)
    Typus.application(name)
  end

  def mango_pay_account_id
    if mango_pay_account = self.mango_pay_account
      mango_pay_account.data['Id']
    else
      nil
    end
  end

  def send_welcome_email
    UserNotifier.signup(self).deliver_now
  end

  def can? action, model
    true
  end

  def cannot? action, model
    false
  end

  def is_not_root?
    false
  end
end
