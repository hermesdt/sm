class ListingLocation
  include Mongoid::Document

  field :type, type: String, default: 'Point'
  field :location, type: Array
  field :listing_id, type: Integer

  index(location: '2dsphere')
  index({ listing_id: 1 }, unique: true)

  def get_listing
    Listing.find_by(id: listing_id)
  end
end
