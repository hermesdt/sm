# == Schema Information
#
# Table name: bank_accounts
#
#  id         :integer          not null, primary key
#  iban       :string           not null
#  bic        :string           not null
#  city       :string           not null
#  country    :string           not null
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_bank_accounts_on_user_id  (user_id)
#

class BankAccount < ActiveRecord::Base
  belongs_to :user

  validates_format_of :bic, with: /\A[a-zA-Z]{6}\w{2}(\w{3})?\Z/, allow_blank: true
  validates_format_of :iban, with: /\A[a-zA-Z]{2}\d{2}\s*(\w{4}\s*){2,7}\w{1,4}\s*\Z/, allow_blank: true
end
