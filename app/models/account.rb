require 'ostruct'

class Account < OpenStruct
  include ActiveModel::Validations

  validates :email, :first_name, :last_name, :birthday,
    :nationality, :country_of_residence, presence: true

  def birthday
    year = self.send(:'birthday(1i)')
    month = self.send(:'birthday(2i)')
    day = self.send(:'birthday(3i)')
    return if year.blank? || month.blank? || day.blank?

    Date.parse("#{year}-#{month}-#{day}")
  end

  def to_mango_params
    {
        FirstName: first_name,
        Email: email,
        Birthday: birthday.to_datetime.to_i,
        LastName: last_name,
        Nationality: nationality,
        CountryOfResidence: country_of_residence
    }
  end

    #   MangoPay::NaturalUser.create({
    #   Tag: 'Test natural user',
    #   Email: 'my@email.com',
    #   FirstName: 'John',
    #   LastName: 'Doe',
    #   Address: {
    #     AddressLine1: 'Le Palais Royal',
    #     AddressLine2: '8 Rue de Montpensier',
    #     City: 'Paris',
    #     Region: '',
    #     PostalCode: '75001',
    #     Country: 'FR'
    #   },
    #   Birthday: 1300186358,
    #   Birthplace: 'Paris',
    #   Nationality: 'FR',
    #   CountryOfResidence: 'FR',
    #   Occupation: 'Worker',
    #   IncomeRange: 1
    # })
end