require 'ostruct'

class Search < OpenStruct
  def initialize options
    if options[:start_date].blank? || options[:end_date].blank?
      options[:start_date] = options[:end_date] = nil
    end

    if options[:lat].blank? || options[:lng].blank?
      options[:lat] = options[:lng] = nil
    end

    super(options)
  end

end