# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160203173253) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "iban",       null: false
    t.string   "bic",        null: false
    t.string   "city",       null: false
    t.string   "country",    null: false
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bank_accounts", ["user_id"], name: "index_bank_accounts_on_user_id", using: :btree

  create_table "image_data", force: :cascade do |t|
    t.binary   "data",           null: false
    t.string   "content_type",   null: false
    t.integer  "imageable_id",   null: false
    t.string   "imageable_type", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "listings", force: :cascade do |t|
    t.string   "name",                   null: false
    t.integer  "user_id"
    t.text     "description",            null: false
    t.money    "daily_price",  scale: 2, null: false
    t.money    "weekly_price", scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat",                    null: false
    t.float    "lng",                    null: false
    t.string   "address",                null: false
    t.datetime "deleted_at"
  end

  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",        default: ""
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "reservation_id"
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "mango_pay_accounts", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.jsonb    "data",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mango_pay_accounts", ["user_id"], name: "index_mango_pay_accounts_on_user_id", using: :btree

  create_table "mango_pay_bank_accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.jsonb    "data",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mango_pay_bank_accounts", ["user_id"], name: "index_mango_pay_bank_accounts_on_user_id", using: :btree

  create_table "mango_pay_wallets", force: :cascade do |t|
    t.integer  "user_id"
    t.jsonb    "data",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "mango_pay_wallets", ["user_id"], name: "index_mango_pay_wallets_on_user_id", using: :btree

  create_table "reservations", force: :cascade do |t|
    t.integer  "creator_id",                             null: false
    t.integer  "owner_id",                               null: false
    t.integer  "status",                 default: 0,     null: false
    t.integer  "listing_id",                             null: false
    t.date     "start_date",                             null: false
    t.date     "end_date",                               null: false
    t.money    "amount",       scale: 2,                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "payment_data",           default: {},    null: false
    t.boolean  "transfered",             default: false
    t.datetime "deleted_at"
  end

  add_index "reservations", ["creator_id", "status"], name: "index_reservations_on_creator_id_and_status", using: :btree
  add_index "reservations", ["creator_id"], name: "index_reservations_on_creator_id", using: :btree
  add_index "reservations", ["end_date"], name: "index_reservations_on_end_date", using: :btree
  add_index "reservations", ["listing_id"], name: "index_reservations_on_listing_id", using: :btree
  add_index "reservations", ["owner_id", "status"], name: "index_reservations_on_owner_id_and_status", using: :btree
  add_index "reservations", ["owner_id"], name: "index_reservations_on_owner_id", using: :btree
  add_index "reservations", ["start_date"], name: "index_reservations_on_start_date", using: :btree
  add_index "reservations", ["status"], name: "index_reservations_on_status", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "name",       null: false
    t.text     "value",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["name"], name: "index_settings_on_name", using: :btree

  create_table "stripe_accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.jsonb    "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "stripe_accounts", ["user_id"], name: "index_stripe_accounts_on_user_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "listing_id"
    t.date     "start_date",           null: false
    t.date     "end_date",             null: false
    t.money    "price",      scale: 2
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "transactions", ["listing_id"], name: "index_transactions_on_listing_id", using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.text     "image"
    t.integer  "phone"
    t.datetime "deleted_at"
    t.float    "lat"
    t.float    "lng"
    t.string   "address"
    t.string   "postal_code"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "bank_accounts", "users"
  add_foreign_key "listings", "users"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "mango_pay_accounts", "users"
  add_foreign_key "mango_pay_bank_accounts", "users"
  add_foreign_key "mango_pay_wallets", "users"
  add_foreign_key "reservations", "listings"
  add_foreign_key "reservations", "users", column: "creator_id"
  add_foreign_key "reservations", "users", column: "owner_id"
  add_foreign_key "stripe_accounts", "users"
  add_foreign_key "transactions", "listings"
  add_foreign_key "transactions", "users"
end
