class CreateMangoPayBankAccounts < ActiveRecord::Migration
  def change
    create_table :mango_pay_bank_accounts do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.jsonb :data, null: false

      t.timestamps
    end
  end
end
