class AddColumnsLatitudeLongitudeToTableListings < ActiveRecord::Migration
  def change
    add_column :listings, :lat, :float, null: false
    add_column :listings, :lng, :float, null: false
    add_column :listings, :address, :string, null: false
  end
end
