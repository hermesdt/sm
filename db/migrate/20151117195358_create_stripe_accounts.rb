class CreateStripeAccounts < ActiveRecord::Migration
  def change
    create_table :stripe_accounts do |t|
      t.belongs_to :user, index: true, foreign_key: true, unique: true
      t.jsonb :data

      t.timestamps null: false
    end
  end
end
