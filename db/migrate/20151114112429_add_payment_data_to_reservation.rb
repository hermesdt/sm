class AddPaymentDataToReservation < ActiveRecord::Migration
  def change
    add_column :reservations, :payment_data, :json, null: false, default: {}
    add_column :reservations, :transfered, :boolean, default: false
  end
end
