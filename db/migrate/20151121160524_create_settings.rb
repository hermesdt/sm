class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :name, null: false, index: true
      t.text :value, null: false

      t.timestamps
    end
  end
end
