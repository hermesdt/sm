class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :listing, index: true, foreign_key: true
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.money :price

      t.timestamps null: false
    end
  end
end
