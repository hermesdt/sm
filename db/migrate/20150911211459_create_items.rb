class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name, null: false
      t.belongs_to :user, index: true, foreign_key: true
      t.text :description
      t.money :hourly_price
      t.money :daily_price
      t.money :weekly_price

    end
  end
end
