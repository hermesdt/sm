class AddReservationIdToMailboxerConversation < ActiveRecord::Migration
  def change
    add_column :mailboxer_conversations, :reservation_id, :integer, index: true, foreign_key: true
  end
end
