class RemoveColumnHourlyPriceFromListings < ActiveRecord::Migration
  def change
    remove_column :listings, :hourly_price, :money, null: false
  end
end
