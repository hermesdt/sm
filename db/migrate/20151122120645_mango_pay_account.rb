class MangoPayAccount < ActiveRecord::Migration
  def change
    create_table :mango_pay_accounts do |t|
      t.belongs_to :user, index: true, foreign_key: true, null: false
      t.jsonb :data, null: false

      t.timestamps
    end
  end
end
