class AddDeletedAtToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :deleted_at, :datetime, index: true
  end
end
