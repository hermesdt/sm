class CreateBankAccountsTable < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.string :iban, null: false
      t.string :bic, null: false
      t.string :city, null: false
      t.string :country, null: false
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
