class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :creator_id, null: false
      t.integer :owner_id, null: false
      t.integer :status, null: false, default: 0
      t.integer :listing_id, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.money :amount, null: false

      t.timestamps

      t.index :creator_id
      t.index :listing_id
      t.index :start_date
      t.index :end_date
      t.index [:creator_id, :status]
      t.index :owner_id
      t.index [:owner_id, :status]
      t.index :status
    end

    add_foreign_key :reservations, :users, column: :creator_id
    add_foreign_key :reservations, :users, column: :owner_id
    add_foreign_key :reservations, :listings, column: :listing_id
  end
end
