class ChangeListingsNulls < ActiveRecord::Migration
  def change
    change_column :listings, :description, :text, null: false
    change_column :listings, :hourly_price, :money, null: false
    change_column :listings, :daily_price, :money, null: false
    change_column :listings, :weekly_price, :money, null: false
  end
end
