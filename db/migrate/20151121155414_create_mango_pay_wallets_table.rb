class CreateMangoPayWalletsTable < ActiveRecord::Migration
  def change
    create_table :mango_pay_wallets do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.jsonb :data, null: false

      t.timestamps null: false
    end
  end
end
