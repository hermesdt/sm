module OmniauthFacebookTestHelper
  def create_auth_params
    OmniAuth::AuthHash.new(provider: 'facebook', uid: 11110000,
      password: Devise.friendly_token[0,20],
      info: {email: "someemail+#{rand(100_000)}@gmail.com",
        name: 'some name',
        image: 'some_image.jpg'}) 
  end

  def create_ie_auth_params
    OmniAuth::AuthHash.new(provider: 'facebook', uid: 11110000,
      password: Devise.friendly_token[0,20],
      info: {email: "someemail+#{rand(100_000)}@student.ie.edu",
        name: 'some name',
        image: 'some_image.jpg'}) 
  end
end