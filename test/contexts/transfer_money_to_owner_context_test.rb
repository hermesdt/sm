require 'test_helper'
require 'ostruct'

class TransferMoneyToOwnerContextTest < ActiveSupport::TestCase
  test "should return 2800" do
    Setting[Setting::FEE_KEY] = 10.0
    context = TransferMoneyToOwnerContext.new(
      reservation: Reservation.new(amount: 28.0)
    )
    assert_equal context.transfer_amount, "1800"
  end

  test "should run all context correctly" do
    Setting[Setting::FEE_KEY] = 10.0
    context = TransferMoneyToOwnerContext.new(
      reservation: Reservation.new(amount: 28.0)
    )
    context.stubs(:get_owner_wallet).returns(:wallet).times(1)
    context.stubs(:send_payment_to_mango_pay).with(:wallet).times(1)

    context.call
    
    assert_equal context.transfer_amount, "1800"
  end
end