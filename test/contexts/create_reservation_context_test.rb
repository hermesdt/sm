require 'test_helper'
require 'ostruct'

class CreateReservationContextTest < ActiveSupport::TestCase
  test "should raise same user exception" do
    user = users(:one)
    listing = listings(:bike)
    reservation_params = {:start_date => '2015-10-10', :end_date => '2015-10-12'}

    context = CreateReservationContext.new(user, listing, reservation_params)
    assert_raises CreateReservationContext::SameUserException do
      context.call
    end
  end

  test "should raise item reserved exception on dates intersection" do
    user = users(:two)
    listing = listings(:bike)
    reservation = reservations(:one)
    reservation_params = {:start_date => (reservation.start_date.to_date + 1.day),
      :end_date => (reservation.end_date.to_date + 10.days)}

    context = CreateReservationContext.new(user, listing, reservation_params)
    assert_raises CreateReservationContext::ItemReservedException do
      context.call
    end
  end  

  test "should raise item reserved exception on dates inclusion" do
    user = users(:two)
    listing = listings(:bike)
    reservation = reservations(:one)
    reservation_params = {:start_date => (reservation.start_date.to_date - 3.day),
      :end_date => (reservation.end_date.to_date + 10.days)}

    context = CreateReservationContext.new(user, listing, reservation_params)
    assert_raises CreateReservationContext::ItemReservedException do
      context.call
    end
  end

  test "should create reservation successfuly" do
    user = users(:two)
    listing = listings(:bike)
    start_date = '2015-10-10'.to_date
    end_date = '2015-10-12'.to_date
    reservation_params = {:listing_id => listing.id,
      :start_date => start_date, :end_date => end_date}

    context = CreateReservationContext.new(user, listing, reservation_params)
    assert_difference "Reservation.count" do
      context.call
    end

    days = reservation_params[:end_date] - reservation_params[:start_date]
    reservation = Reservation.last 
    assert_equal Reservation.statuses[reservation.status], Reservation.statuses[:pending]
    assert_equal reservation.owner_id, listing.user_id
    assert_equal reservation.creator_id, user.id

    weeks = (end_date - start_date).to_i / 7
    days = (end_date - start_date).to_i % 7
    amount = days * listing.daily_price + weeks * listing.weekly_price

    assert_equal reservation.amount, amount
  end

end