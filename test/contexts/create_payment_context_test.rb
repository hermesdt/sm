require 'test_helper'
require 'ostruct'

class CreatePaymentContextTest < ActiveSupport::TestCase
  test "should raise NotAcceptedStatusError" do
    context = CreatePaymentContext.new(
      reservation: Reservation.new(status: Reservation.statuses[:paid])
    )

    assert_raises CreatePaymentContext::NotAcceptedStatusError do
      context.call
    end
  end
end