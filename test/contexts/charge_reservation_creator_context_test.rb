require 'test_helper'
require 'ostruct'

class ChargeReservationCreatorContextTest < ActiveSupport::TestCase
  test "should return 2800" do
    context = ChargeReservationCreatorContext.new(
      reservation: Reservation.new(amount: 28.0)
    )
    assert_equal context.reservation_amount, "2800"
  end

  test "should run all context correctly" do
  end
end