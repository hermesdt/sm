require 'test_helper'
require 'ostruct'
require 'support/omniauth_facebook_test_helper'

class UserFacebookAuthContextTest < ActiveSupport::TestCase
  include OmniauthFacebookTestHelper

  test "should create ie user" do
    users_count = User.count
    auth_params = create_ie_auth_params

    user = UserFacebookAuthContext.new(auth_params).call

    assert_equal user.email, auth_params.info.email
    assert User.count == users_count + 1
  end

  test "should not create user because is not IE domain" do
    users_count = User.count
    auth_params = create_auth_params

    user = UserFacebookAuthContext.new(auth_params).call

    assert_equal user.email, auth_params.info.email
    assert User.count == users_count
  end

  test "should not create user" do
    auth_params = create_auth_params
    user1 = UserFacebookAuthContext.new(auth_params).call

    users_count = User.count
    user2 = UserFacebookAuthContext.new(auth_params).call

    assert User.count == users_count
    assert_equal user1.email, user2.email
  end
end