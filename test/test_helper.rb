ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/hooks/test'
require 'minitest/autorun'
require "mocha/mini_test"

class ActiveSupport::TestCase
  include Minitest::Hooks

  fixtures :all

  ActiveRecord::Migration.check_pending!

  DatabaseCleaner.clean_with :truncation
  DatabaseCleaner.strategy = :transaction

  setup do
    DatabaseCleaner.start
  end

  teardown do
    DatabaseCleaner.clean
  end

  def assert_string_contains container, contained
    assert_equal(container[contained], contained)
  end
end
