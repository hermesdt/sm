require 'test_helper'

class CardTest < ActiveSupport::TestCase
  test "must result correct date" do
    date = Card.new(expiration_month: '1', expiration_year: '2016').expiration_date
    assert_equal date, '0116'
  end

  test "must raise error because of month not integer" do
      date = Card.new(expiration_month: '', expiration_year: '2016').expiration_date
      assert_equal date, ""
  end

  test "must raise error because of year not integer" do
    date = Card.new(expiration_month: '', expiration_year: nil).expiration_date
    assert_equal date, ""
  end
end
