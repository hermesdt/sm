# == Schema Information
#
# Table name: reservations
#
#  id           :integer          not null, primary key
#  creator_id   :integer          not null
#  owner_id     :integer          not null
#  status       :integer          default(0), not null
#  listing_id   :integer          not null
#  start_date   :date             not null
#  end_date     :date             not null
#  amount       :money            not null
#  created_at   :datetime
#  updated_at   :datetime
#  payment_data :json             default({}), not null
#  transfered   :boolean          default(FALSE)
#  deleted_at   :datetime
#
# Indexes
#
#  index_reservations_on_creator_id             (creator_id)
#  index_reservations_on_creator_id_and_status  (creator_id,status)
#  index_reservations_on_end_date               (end_date)
#  index_reservations_on_listing_id             (listing_id)
#  index_reservations_on_owner_id               (owner_id)
#  index_reservations_on_owner_id_and_status    (owner_id,status)
#  index_reservations_on_start_date             (start_date)
#  index_reservations_on_status                 (status)
#

require 'test_helper'

class ReservationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
