require 'test_helper'

class GetOrCreateMangoPayWalletServiceTest < ActiveSupport::TestCase
  test "service must return present wallet without calling api" do
    wallet = mango_pay_wallets(:one)
    user = users(:one)

    service = GetOrCreateMangoPayWalletService.new(user)
    service.stubs(:create_wallet).times(0)

    assert_equal service.run, wallet
  end

  test "service must create and return new wallet" do
    user = users(:two)
    data = {'param1' => 'param1', 'param2' => 'param2'}

    service = GetOrCreateMangoPayWalletService.new(user)

    service.stubs(:create_wallet).returns(data)
    returned_wallet = service.run

    assert_equal returned_wallet.data, data
  end
end
