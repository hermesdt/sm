require 'test_helper'

class Users::OmniauthCallbacksControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  include OmniauthFacebookTestHelper

  test "should authenticate user" do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    OmniAuth.config.test_mode = true
    auth_params = create_ie_auth_params
    @request.env["omniauth.auth"] = auth_params

    post :facebook

    assert_equal session["warden.user.user.key"][0].first,
      User.where(email: auth_params.info.email).last.id
  end

  test "should not authenticate non ie user" do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    OmniAuth.config.test_mode = true
    auth_params = create_auth_params
    @request.env["omniauth.auth"] = auth_params

    post :facebook

    assert_equal session["warden.user.user.key"], nil
  end
end