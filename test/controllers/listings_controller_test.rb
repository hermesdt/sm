require 'test_helper'

class ListingsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  test "should assign a listing" do
    @listing = listings(:bike)

    get :show, id: @listing.id

    assert_equal assigns["listing"], @listing
  end

  test "should not create anything" do
    assert_difference 'Listing.count', 0 do
      post :create, listing_params
    end

    assert_response :redirect
    assert_equal response.location, new_user_session_url
  end

  test "should create a new listing" do
    user = users(:one)
    user.create_mango_pay_wallet(data: {mango_id: '1'})
    user.create_mango_pay_account(data: {mango_id: '1'})
    user.create_mango_pay_bank_account(data: {mango_id: '1'})

    sign_in user

    assert_difference 'Listing.count' do
      post :create, listing_params
    end
    assert_equal Listing.last.user_id, user.id
  end

  test "should not create a new listing because invalid params" do
    user = users(:one)
    user.create_mango_pay_wallet(data: {mango_id: '1'})
    user.create_mango_pay_account(data: {mango_id: '1'})
    user.create_mango_pay_bank_account(data: {mango_id: '1'})

    sign_in user

    params = listing_params
    params[:listing].delete(:name)

    assert_difference 'Listing.count', 0 do
      post :create, params
    end
  end

  test "should not create a new listing because dont have mangopay data" do
    user = users(:one)
    sign_in user

    post :create, listing_params
    assert_response :redirect
    assert_equal response.location, new_mango_pay_account_url(:next_step => new_listing_path)
  end

  test "should redirect because is not logged in" do
    delete :destroy, id: 1

    assert_response :redirect
    assert_equal response.location, new_user_session_url
  end

  test "should redirect because not exists" do
    sign_in(users(:one))
    listings(:moto)

    delete :destroy, id: 2

    assert_response :redirect
    assert_equal response.location, root_url
    assert_equal flash[:error], I18n.t("flash.unauthorized")
  end

  test "should destroy listing 1" do
    listing = listings(:bike)
    sign_in(users(:one))
    delete :destroy, id: listing.id

    assert_response :redirect
    assert_equal response.location, dashboard_url
  end

  def listing_params
    {listing: {name: 'item 1', description: 'some description',
      lat: 40, lng: -3, address: 'Sol, Madrid',
      daily_price: 1, weekly_price: 1, image_data_attributes: {raw_data: fixture_file_upload("product_example.jpg", "image/jpeg")}}}
  end
end