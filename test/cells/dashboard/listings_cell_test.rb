require 'test_helper'

class Dashboard::ListingsCellTest < Cell::TestCase
  controller DashboardController

  test "show" do
    bike = listings(:bike)

    html = cell('dashboard/listings', bike).(:show)
    assert_string_contains html, bike.name
  end

  test "empty action" do
    html = cell('dashboard/listings').(:empty)
    assert_string_contains html, I18n.t("dashboard.listings.empty.title")
    assert_string_contains html, I18n.t("dashboard.listings.empty.add_one")
  end
end
