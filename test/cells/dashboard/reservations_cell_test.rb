require 'test_helper'

class Dashboard::ReservationsCellTest < Cell::TestCase
  controller DashboardController

  test "show" do
    reservation = reservations(:paid)
    html = cell("dashboard/reservations", reservation).(:show)
    assert html.match /#{reservation.listing.name}/
  end

  test "empty action" do
    html = cell('dashboard/reservations').(:empty)
    assert_string_contains html, I18n.t("dashboard.reservations.empty.title")
    assert_string_contains html, I18n.t("dashboard.reservations.empty.no_rentals")
  end
end
