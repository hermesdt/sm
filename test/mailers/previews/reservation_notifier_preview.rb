class ReservationNotifierPreview < ActionMailer::Preview
  def created_owner
    ReservationNotifier.created_owner(User.first, Reservation.first)
  end

  def created_creator
    ReservationNotifier.created_creator(User.first, Reservation.first)
  end

  def accepted_owner
    ReservationNotifier.accepted_owner(User.first, Reservation.first)
  end

  def accepted_creator
    ReservationNotifier.accepted_creator(User.first, Reservation.first)
  end

  def rejected_owner
    ReservationNotifier.rejected_owner(User.first, Reservation.first)
  end

  def rejected_creator
    ReservationNotifier.rejected_creator(User.first, Reservation.first)
  end

  def paid_owner
    ReservationNotifier.paid_owner(User.first, Reservation.first)
  end

  def paid_creator
    ReservationNotifier.paid_creator(User.first, Reservation.last)
  end
end