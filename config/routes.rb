Rails.application.routes.draw do

  get '/contents/:section' => 'contents#section'
  get '/initials/:id' => 'initials#show', as: :initial
  devise_for :users, controllers: {
    omniauth_callbacks: "users/omniauth_callbacks",
    registrations: 'users/registrations',
    confirmations: 'users/confirmations'
  }

  get '/pages/:id' => 'pages#show'
  resources :mango_pay_accounts, only: [:new, :create]
  resources :bank_accounts, only: [:new, :create]
  resources :payments, only: [:new, :create]
  get '/stripe_accounts/callback' => 'stripe_accounts#callback'

  resources :searches, only: [:index] do
    collection do
      get :get_distances
    end
  end
  resources :conversations, only: [:index, :show, :destroy] do
    member do
      post :restore
      post :reply
      post :mark_as_read
    end

    collection do
      delete :empty_trash
    end
  end
  resources :images, only: [:show]
  resources :users, only: [:show]
  resources :reservations, only: [:create] do
    member do
      get :request_payment
      post :pay
      post :accept
      post :reject
    end
  end
  resources :listings, only: [:show, :edit, :update, :new, :create, :destroy]
  get '/dashboard' => 'dashboard#index'
  get '/change_locale/:locale' => 'root#change_locale', as: :change_locale
  root 'root#index'

  # Static pages
  get '/how_it_works' => 'pages#how_it_works'
  get '/faq' => 'pages#faq'
  get '/trust_and_safety' => 'pages#trust_and_safety'
  get '/about_us' => 'pages#about_us'
  get '/privacy_policy' => 'pages#privacy_policy'
  get '/terms_of_use' => 'pages#terms_of_use'
  get '/contact_us' => 'pages#contact_us'
  get '/promotion_fb' => 'pages#promotion_fb'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
