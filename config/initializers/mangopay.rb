client_id, client_passphrase = if Rails.env.development?
  config = YAML.load(File.read('config/mangopay.yml'))
  [ config["client_id"], config["client_passphrase"] ]
else
  [ ENV['MANGOPAY_CLIENT_ID'], ENV['MANGOPAY_CLIENT_PASSPHRASE'] ]
end

MangoPay.configure do |c|
  c.preproduction = true
  c.client_id = client_id
  c.client_passphrase = client_passphrase
end